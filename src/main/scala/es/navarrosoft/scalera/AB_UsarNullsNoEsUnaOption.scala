package es.navarrosoft.scalera

/**
  * Un Option es un tipo parametrizado que puede tener dos posibles valores:
  * Some(T)
  * None
  * Podemos encapsular los nulls en Option, de manera que en vez de trabajar directamente con nulls, trabajamos con un
  * tipo preparado para devolver valor nulos.
  * Option(null) === None  (nota: === es la comparación estricta)
  * No más explosiones de punteros a null o comprobaciones del tipo if myValue == null.
  */
object AB_UsarNullsNoEsUnaOption extends App {

  // Imaginemos el típico método que devuelve un valor de un mapa:
  val m =
    Map(
      1 -> "el breikin dance",
      2 -> "el crusaito",
      3 -> "el maikel jacson",
      4 -> "el robocó"
    )

  // Si no trabajásemos con Option tendríamos que comprobar si una clave existe antes de pedir su valor.
  if(m.contains(1))
    println("El paso de baile es: " + m(1))
  else
    println("No existe ese paso de baile")
  // Sin embargo, podemos pedirle al mapa que nos devuelva el valor directamente en un Option, y en caso de que no
  // exista, este Option será None.
  println(m get 1) //Devolverá Some("el breikin dance")
  println(m get 5) //Devolverá None
  // ¿Cómo se trabaja con el tipo Option?
  // Existen varias formas de trabajar con esta estructura tan maja.
  // Existe el método getOrElse. Este método devolverá el valor del Option (en caso de ser un Some) o bien devolverá
  // lo que le pasemos como argumento a este método. Un ejemplos sencillo:
  println(m getOrElse(1, "Paquito el chocolatero"))
  // Otra posibilidad es utilizar Pattern Matching:
  m get 1 match {
    case Some(danceStep) =>
      println("Soy el amo de la pista bailando: " + danceStep)
    case None =>
      println("Soy de los que me quedo mirando en la barra")
  }
  /**
    * También es posible utilizar el método fold. El método fold, a diferencia del método getOrElse (y al igual que el
    * pattern matching), permite aplicar alguna función al método devuelto. El primer argumento de la función
    * (llamado ifEmpty) recibirá el resultado que se devolverá en caso de que el Option sea None. El segundo argumento
    * recibe una función, la cual definiremos para saber que resultado devolver en caso de que el Option tenga un valor.
    */
  (m get 1).fold(
    ifEmpty = println("A mi lo de bailar no me va mucho")){
    step => println(s"Me rompí la cadera bailando $step")
  }
  // Y para los functional lovers (un saludo para ellos), no hay que olvidar que un Option es una mónada y podemos
  // hacer transformaciones, por ejemplo con map, flatmaps o, aplicando un poco de azucar sintáctico, for comprehension:
  for {
    first <- m.get(1)
    second <- m.get(2)
  } yield println(s"Empiezo con $first y termino con $second")
}
