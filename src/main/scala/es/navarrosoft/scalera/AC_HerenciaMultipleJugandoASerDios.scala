package es.navarrosoft.scalera

/**
  * Una de las características de Scala más importantes es la posibilidad de hacer mixin.
  * Mediante mixin podemos tener herencia múltiple de varios traits.
  * ¿Qué son los traits?
  * Los Traits (o rasgos en la lengua de Cervantes) son un aglutinamiento de atributos y
  * métodos propios de un rasgo o característica. Son similares a las interfaces de Java
  * con la salvedad de que se pueden implementar parcialmente, es decir, solo algunos de
  * sus métodos o valores.
  * Sirve para añadir comportamientos a otras entidades. Por ejemplo, vamos a implementar
  * el trait CanFly en el que definiremos los valores y métodos necesarios para cualquier
  * criatura voladora.
  */
object AC_HerenciaMultipleJugandoASerDios extends App {

  trait CanFly {

    val altitude: Float

    def fly() = println("I'm flying!!!")
  }

//  val myFlyingAnimal = new CanFly {} // Error de compilación

  val myFlyingAnimal = new CanFly { val altitude = 100.0f } // Hay que darle valor a la variable

  /**
    * ¿Cómo podemos utilizar los traits?
    * Los traits se usan para añadir comportamientos a nuestras entidades. Para ello podemos,
    * mediante composición, hacer mixin de uno o varios traits a la hora de definir otro trait,
    * una clase, una case class …
    * Para ello, se utilizan dos palabras reservadas: extends y with. El primer trait tendrá
    * como predecesor la palabra extends, el resto se irán incluyendo con with.
    */
  abstract class Superhero extends CanFly

  abstract case class Superman() extends Superhero with CanFly

  /**
    * ¿Y cómo trabajan los mixin con el problema del diamante?
    * Primero vamos a crear un escenario con el problema. Tenemos un trait Animal que tiene un
    * método sin implementar que devuelve el sonido del animal.
    */
  trait Animal {
    def sound: String
  }
  // Por otro lado, tenemos dos especificaciones del animal: un gato y un perro.
  trait Cat extends Animal {
    override def sound = "Miau"
  }

  trait Dog extends Animal {
    override def sound = "Guau"
  }

  /**
    * Haciendo un esfuerzo de imaginación sin igual, vamos a imaginar que un zorro es la
    * mezcla entre un gato y un perro.
    */
  case class Fox() extends Cat with Dog
  // Pero, si intentamos imprimir el sonido del zorro….
  val myFox = Fox()
  println("El zorro dice " + myFox.sound)
  /**
    * Pues en este caso, nuestro zorro ladrará. ¿Por qué? Por que el último trait con el
    * que se ha hecho mixin es Dog. Primer se utiliza la implementación de Cat, pero más
    * tarde es sobrescrita por el siguiente trait definido, es decir, Dog. Por tanto podemos
    * decir, que los traits se van instanciando en el orden en el que se han declarado.
    * ¡Y ya está! ¿Qué hemos aprendido hoy? Que los traits y los mixins son bastante útiles….
    * y que los zorros, en ocasiones, ladran.
    */
}
