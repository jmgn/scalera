package es.navarrosoft.scalera

/**
  *
  */
object AE_ClasesAnonimas extends App {

  /**
    * ¿cómo puedo crear una instancia mediante una clase anónima? Muy fácil.
    * Simplemente con la palabra reservada new y definiendo el cuerpo con llaves.
    */
  val myPoint = new{ val x = 1; val y = 2 }
  println("El punto tiene por coordendas (" + myPoint.x + "," + myPoint.y + ")")
  /**
    * De esta forma creamos una instancia que contiene dos valores enteros: x e y.
    * Sin embargo, como se puede apreciar, no hemos dado nombre a esta clase.
    *
    * Azucar sintáctico
    * Hace un par de semanas hablamos de los Traits, y de como podíamos crear una
    * instancia de un trait a traves de una clase anónima. Quizás en ese momento,
    * un WTF del tamaño de Sebastopol apareció en tu cabeza. Si recordamos aquel post,
    * la sintaxis para crear una instancia desde un trait era algo así:
    */
  trait AnonymousHero {
    def superpower: String
  }

  val myHero = new AnonymousHero {
    def superpower = "I can compile Scala with my brain"
  }
  println(myHero.superpower)
  /**
    * ¡Estamos creando una instancia desde un trait! ¡No hay clases por ningún lado!
    * ¿Esto es magia negra? Pues no, es sintactic sugar.
    * Realmente, lo que ocurre por debajo es algo similar a esto:
    */
  class AnonymousHeroClass extends AnonymousHero {
    def superpower: String = "I can compile Scala with my brain"
  }

  val myHero2 = new AnonymousHeroClass
  println(myHero2.superpower)
  /**
    * Como se puede ver, a la hora de instanciar un trait, realmente se crea una clase
    * que extiende de dicho trait. Después, se crea una nueva instancia de la clase creada.
    *
    * De esta manera se pueden crear instancias de un trait sin generar boilerplate.
    *
    * Bonus-track: Funciones anónimas
    * En Scala es posible utilizar expresiones lambda, es decir, funciones anónimas. Más adelante,
    * en otros posts, veremos la importancia del uso de funciones anónimas, por ejemplo, en métodos
    * que admiten funciones como parámetro.
    * Las funciones anónimas, al igual que las clases anónimas, son funciones que no es necesario declarar.
    * Un ejemplo de función anónima es la siguiente:
    */
  (x: Int) => x + 1
  /**
    * En este caso, la función anónima espera un entero y devolverá ese entero sumado una unidad.
    * No hay que olvidar que, en Scala, las funciones realmente son objetos. Y teniendo en cuenta esto último,
    * podemos relacionar las funciones anónimas (o expresiones lambda) con clases anónimas.
    * Realmente, cuando se utiliza la función anónima (x: Int) => x + 1 lo que se está haciendo por debajo es
    * crear una instancia de la clase anónima utilizando el trait Function1:
    * new Function1[Int, Int] {
    *     def apply(x: Int): Int = x + 1
    * }
    */



}
