package es.navarrosoft.scalera

/**
  * Traversable se considera la base para todas las colecciones de Scala y se caracteriza principalmente por dos rasgos:
  * · Strictness: Todos los elementos tienen que ser computados previamente para poder manejarlos como valores.
  * · Orderness: Los elementos mantienen una relación de orden entre ellos
  */
object AA_TraversableOpsFold extends App {

  /**
    * Supongamos que queremos concatenar los números del 1 al 5 en una cadena de texto, obteniendo el resultado
    * “My numbers are: 12345” ¿cuál sería la primera aproximación? Podría ser algo del estilo:
    */
  var initial: String = "My numbers are: "
  for(i <- 1 to 10){
    initial = initial + i.toString
  }

  println(initial)

  /**
    * La operación que os proponemos para sustituir el bucle anterior es fold, que tiene dos variantes: foldLeft y
    * foldRight. Mientras que la primera operación ejecuta elemento a elemento del Traversable de manera iterativa,
    * la segunda lo hace de manera recursiva, por tanto se recomienda evitarla si la colección sobre la que ejecutamos
    * el foldRight tiene un tamaño considerable. Nos centraremos pues en la primera operación. La notación del foldLeft
    * para un cierto Traversable[T] es: def foldLeft[U](zero: U)(f: (U,T) => U): U
    * dicho así puede parecer dificil de ver. Apliquemos la operación al ejemplo anterior:
    * El elemento ‘zero’ en este caso, sería la cadena inicial de tipo String “My numbers are: ” Y la función a aplicar
    * para cada par de String, Int sería la concatenación.
    * Por tanto seríamos capaz de expresar el primer snippet de código, sin hacer uso de variables, de la siguiente
    * forma:
    */

  // With fold
  val myNumbersFold = (1 to 10).foldLeft("My numbers are: ")((accumulator,iterator) => accumulator + iterator.toString)

  println(myNumbersFold)

  // O podemos no escribir el fold con azúcar sintáctico

  val myNumbersSugarSyntax =
    ("My numbers are: " /: (1 to 10))((accumulator,iterator) => accumulator + iterator.toString)

  println(myNumbersSugarSyntax)
}
